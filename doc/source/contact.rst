=======
Contact
=======

QEpy Developers
================

Feel free to contact the developers:
 - `Xuecheng Shao <https://sites.rutgers.edu/prg/people/xuecheng-shao/>`_
 - `Michele Pavanello <https://sasn.rutgers.edu/about-us/faculty-staff/michele-pavanello>`_

On-line
=======

To find out more about the `Pavanello Research Group <http://sites.rutgers.edu/prg>`_. Or simply send us an email.

GitLab
======

Feel free to create new issues, merge requests or fork your own `QEpy` on our gitlab page: https://gitlab.com/shaoxc/qepy

Make sure to let us know about your developments!

References
==========

QEpy
----
.. literalinclude:: QEpy.bib
    :lines: 1-7

Quantum ESPRESSO
================
.. literalinclude:: QEpy.bib
    :lines: 9-25
