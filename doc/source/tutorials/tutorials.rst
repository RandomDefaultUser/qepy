.. _tutorials:

=========
Tutorials
=========

SCF
===

`Simple SCF job <jupyter/qepy_scf.ipynb>`__.
--------------------------------------------

`Iteratively run SCF <jupyter/qepy_scf_iterative.ipynb>`__.
-----------------------------------------------------------
